package az.ingress.marketappp.mapper;

import az.ingress.marketappp.dto.BranchDto;
import az.ingress.marketappp.dto.BranchDto.BranchDtoBuilder;
import az.ingress.marketappp.dto.CreateMarketDto;
import az.ingress.marketappp.dto.CreatePhoneDto;
import az.ingress.marketappp.dto.CreatePhoneDto.CreatePhoneDtoBuilder;
import az.ingress.marketappp.dto.MarketDto;
import az.ingress.marketappp.dto.MarketDto.MarketDtoBuilder;
import az.ingress.marketappp.model.Branch;
import az.ingress.marketappp.model.Market;
import az.ingress.marketappp.model.Market.MarketBuilder;
import az.ingress.marketappp.model.Phone;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-26T12:41:57+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 20.0.2.1 (Amazon.com Inc.)"
)
@Component
public class MarketMapperImpl implements MarketMapper {

    @Override
    public Market dtoToMarket(CreateMarketDto dto) {
        if ( dto == null ) {
            return null;
        }

        MarketBuilder market = Market.builder();

        market.name( dto.getName() );
        market.address( dto.getAddress() );

        return market.build();
    }

    @Override
    public MarketDto marketToDto(Market market) {
        if ( market == null ) {
            return null;
        }

        MarketDtoBuilder marketDto = MarketDto.builder();

        marketDto.id( market.getId() );
        marketDto.name( market.getName() );
        marketDto.address( market.getAddress() );
        marketDto.branches( branchListToBranchDtoList( market.getBranches() ) );

        return marketDto.build();
    }

    protected CreatePhoneDto phoneToCreatePhoneDto(Phone phone) {
        if ( phone == null ) {
            return null;
        }

        CreatePhoneDtoBuilder createPhoneDto = CreatePhoneDto.builder();

        createPhoneDto.number( phone.getNumber() );

        return createPhoneDto.build();
    }

    protected List<CreatePhoneDto> phoneListToCreatePhoneDtoList(List<Phone> list) {
        if ( list == null ) {
            return null;
        }

        List<CreatePhoneDto> list1 = new ArrayList<CreatePhoneDto>( list.size() );
        for ( Phone phone : list ) {
            list1.add( phoneToCreatePhoneDto( phone ) );
        }

        return list1;
    }

    protected BranchDto branchToBranchDto(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        BranchDtoBuilder<?, ?> branchDto = BranchDto.builder();

        branchDto.name( branch.getName() );
        branchDto.address( branch.getAddress() );
        branchDto.countOfEmployee( branch.getCountOfEmployee() );
        branchDto.phones( phoneListToCreatePhoneDtoList( branch.getPhones() ) );
        branchDto.id( branch.getId() );

        return branchDto.build();
    }

    protected List<BranchDto> branchListToBranchDtoList(List<Branch> list) {
        if ( list == null ) {
            return null;
        }

        List<BranchDto> list1 = new ArrayList<BranchDto>( list.size() );
        for ( Branch branch : list ) {
            list1.add( branchToBranchDto( branch ) );
        }

        return list1;
    }
}
