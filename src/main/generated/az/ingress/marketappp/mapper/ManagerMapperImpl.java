package az.ingress.marketappp.mapper;

import az.ingress.marketappp.dto.CreateManagerDto;
import az.ingress.marketappp.dto.CreatePhoneDto;
import az.ingress.marketappp.model.Manager;
import az.ingress.marketappp.model.Manager.ManagerBuilder;
import az.ingress.marketappp.model.Phone;
import az.ingress.marketappp.model.Phone.PhoneBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-11-26T12:41:57+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 20.0.2.1 (Amazon.com Inc.)"
)
@Component
public class ManagerMapperImpl implements ManagerMapper {

    @Override
    public Manager dtoToManager(CreateManagerDto dto) {
        if ( dto == null ) {
            return null;
        }

        ManagerBuilder manager = Manager.builder();

        manager.name( dto.getName() );
        manager.surname( dto.getSurname() );
        manager.age( dto.getAge() );
        manager.phones( createPhoneDtoListToPhoneList( dto.getPhones() ) );

        return manager.build();
    }

    protected Phone createPhoneDtoToPhone(CreatePhoneDto createPhoneDto) {
        if ( createPhoneDto == null ) {
            return null;
        }

        PhoneBuilder phone = Phone.builder();

        phone.number( createPhoneDto.getNumber() );

        return phone.build();
    }

    protected List<Phone> createPhoneDtoListToPhoneList(List<CreatePhoneDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Phone> list1 = new ArrayList<Phone>( list.size() );
        for ( CreatePhoneDto createPhoneDto : list ) {
            list1.add( createPhoneDtoToPhone( createPhoneDto ) );
        }

        return list1;
    }
}
