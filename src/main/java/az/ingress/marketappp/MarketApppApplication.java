package az.ingress.marketappp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketApppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketApppApplication.class, args);
	}
}
