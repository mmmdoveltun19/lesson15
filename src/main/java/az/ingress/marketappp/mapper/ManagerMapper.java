package az.ingress.marketappp.mapper;

import az.ingress.marketappp.dto.CreateManagerDto;
import az.ingress.marketappp.model.Manager;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ManagerMapper {

    Manager dtoToManager(CreateManagerDto dto);
}

