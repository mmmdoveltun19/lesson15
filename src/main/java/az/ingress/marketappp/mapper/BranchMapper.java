package az.ingress.marketappp.mapper;

import az.ingress.marketappp.dto.CreateBranchDto;
import az.ingress.marketappp.model.Branch;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BranchMapper {

    Branch dtoToBranch(CreateBranchDto dto);
}

