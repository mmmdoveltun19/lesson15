package az.ingress.marketappp.mapper;

import az.ingress.marketappp.dto.CreateMarketDto;
import az.ingress.marketappp.dto.MarketDto;
import az.ingress.marketappp.model.Market;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface MarketMapper {

    Market dtoToMarket(CreateMarketDto dto);

    MarketDto marketToDto(Market market);
}

