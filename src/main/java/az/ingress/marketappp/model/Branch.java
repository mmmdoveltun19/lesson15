package az.ingress.marketappp.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String address;
    Integer countOfEmployee;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(
            name = "branch_phone",
            joinColumns = @JoinColumn(name = "branch_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_id"))
    List<Phone> phones;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "manager_id")
    Manager manager;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "market_id")
    Market market;
}
