package az.ingress.marketappp.controller;

import az.ingress.marketappp.dto.CreateManagerDto;
import az.ingress.marketappp.service.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch/{branchId}/manager")
@Valid
public class ManagerController {

    private final ManagerService managerService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @PathVariable Long branchId,
                       @RequestBody CreateManagerDto managerDto) {
        managerService.create(marketId, branchId, managerDto);
    }
}


