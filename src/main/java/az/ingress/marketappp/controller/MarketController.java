package az.ingress.marketappp.controller;

import az.ingress.marketappp.dto.CreateMarketDto;
import az.ingress.marketappp.dto.MarketDto;
import az.ingress.marketappp.model.Market;
import az.ingress.marketappp.repository.genericsearch.SearchCriteria;
import az.ingress.marketappp.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market")
@Valid
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public void create(@RequestBody CreateMarketDto marketDto) {
        marketService.create(marketDto);
    }

    @PostMapping("/search")
    public Collection<Market> create(@RequestBody List<SearchCriteria> searchCriteria) {
        return marketService.searchByName(searchCriteria);
    }

    @GetMapping
    public List<Market> all() {
        return marketService.findAll();
    }

    @GetMapping("/{id}")
    public MarketDto findById(@PathVariable Long id) {
        return marketService.findById(id);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody CreateMarketDto marketDto) {
        marketService.update(id, marketDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        marketService.delete(id);
    }
}

