package az.ingress.marketappp.controller;

import az.ingress.marketappp.dto.CreateBranchDto;
import az.ingress.marketappp.model.Branch;
import az.ingress.marketappp.repository.genericsearch.SearchCriteria;
import az.ingress.marketappp.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @RequestBody CreateBranchDto branchDto) {
        branchService.create(marketId, branchDto);
    }

    @PostMapping("/search")
    public Page<Branch> search(@PathVariable Long marketId,
                       @RequestBody List<SearchCriteria> searchCriteria,
                       Pageable pageable) {
        return branchService.searchByName(marketId, searchCriteria, pageable);
    }
}