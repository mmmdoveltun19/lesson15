package az.ingress.marketappp.service;

import az.ingress.marketappp.dto.CreateMarketDto;
import az.ingress.marketappp.dto.MarketDto;
import az.ingress.marketappp.model.Market;
import az.ingress.marketappp.repository.genericsearch.SearchCriteria;

import java.util.Collection;
import java.util.List;

public interface MarketService {
    void create(CreateMarketDto marketDto);

    List<Market> findAll();

    MarketDto findById(Long id);

    void update(Long id, CreateMarketDto marketDto);

    void delete(Long id);

    Collection<Market> searchByName(List<SearchCriteria> searchCriteria);

}

