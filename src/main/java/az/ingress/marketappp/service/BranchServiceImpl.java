package az.ingress.marketappp.service;

import az.ingress.marketappp.dto.CreateBranchDto;
import az.ingress.marketappp.mapper.BranchMapper;
import az.ingress.marketappp.model.Branch;
import az.ingress.marketappp.model.Market;
import az.ingress.marketappp.repository.BranchRepository;
import az.ingress.marketappp.repository.MarketRepository;
import az.ingress.marketappp.repository.genericsearch.CustomSpecification;
import az.ingress.marketappp.repository.genericsearch.SearchCriteria;
import jakarta.persistence.criteria.Join;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final BranchMapper branchMapper;

    @Override
    public void create(Long marketId, CreateBranchDto branchDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.dtoToBranch(branchDto);
        branch.setMarket(market);
        branchRepository.save(branch);
    }

    @Override
    public Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable) {
        List<SearchCriteria> newCriteria = searchCriteria.stream()
                .filter(criteria -> !criteria.getKey().equals("name")).toList();
        CustomSpecification<Branch> customSpecification = new CustomSpecification<>(newCriteria);

        Specification<Branch> marketIdSpec = (root, query, criteriaBuilder) -> {
            Join<Branch, Market> marketBranches = root.join("market");
            return criteriaBuilder.equal(marketBranches.get("id"), marketId);
        };
        return branchRepository.findAll(customSpecification.and(marketIdSpec), pageable);
    }
}

