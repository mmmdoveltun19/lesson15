package az.ingress.marketappp.service;

import az.ingress.marketappp.dto.CreateManagerDto;

public interface ManagerService {
    void create(Long marketId, Long branchId, CreateManagerDto managerDto);

}


