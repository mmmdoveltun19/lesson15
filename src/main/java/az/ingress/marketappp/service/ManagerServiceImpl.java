package az.ingress.marketappp.service;

import az.ingress.marketappp.dto.CreateManagerDto;
import az.ingress.marketappp.mapper.ManagerMapper;
import az.ingress.marketappp.model.Manager;
import az.ingress.marketappp.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final BranchRepository branchRepository;
    private final ManagerMapper managerMapper;

    @Override
    public void create(Long marketId, Long branchId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.dtoToManager(managerDto);
        branch.setManager(manager);
        branchRepository.save(branch);
    }

}


