package az.ingress.marketappp.service;


import az.ingress.marketappp.dto.CreateBranchDto;
import az.ingress.marketappp.model.Branch;
import az.ingress.marketappp.repository.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;

public interface BranchService {
    void create(Long marketId, CreateBranchDto branchDto);

    Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable);
}


