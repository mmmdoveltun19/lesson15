package az.ingress.marketappp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarketDto {
    Long id;
    String name;
    String address;
    List<BranchDto> branches;
    List<PhoneDto> phones;
    ManagerDto manager;
}

