package az.ingress.marketappp.repository;

import az.ingress.marketappp.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
}

