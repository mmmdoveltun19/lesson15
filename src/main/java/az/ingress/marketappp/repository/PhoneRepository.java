package az.ingress.marketappp.repository;

import az.ingress.marketappp.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Long> {
}

